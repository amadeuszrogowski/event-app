package io.amicolon;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class EventApp
{
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException
    {
        final ExecutorService executorService = Executors.newFixedThreadPool(2);

        final DataReader dataReader = new DataReader();
        final String eventsFile = args[0];
        final String queriesFile = args[1];

        final Future<List<String>> eventsResult = executorService.submit(() -> dataReader.readFile(eventsFile));
        final Future<List<String>> queriesResult = executorService.submit(() -> dataReader.readFile(queriesFile));

        executorService.shutdown();

        final List<String> eventsInput = eventsResult.get();
        final List<String> queriesInput = queriesResult.get();

        final DataTransformer dataTransformer = new DataTransformer();
        final Map<String, List<Integer>> events = dataTransformer.getEventPropertyValueListGroupedByEventPropertyType(eventsInput);

        final QueriesExecutor queriesExecutor = new QueriesExecutor();
        final String outputFile = args[2];
        queriesExecutor.executeQueries(queriesInput, eventsInput, events, outputFile);
    }

}