package io.amicolon;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class DataReader
{
    List<String> readFile(String fileName) throws IOException, URISyntaxException
    {
        final URL file = this.getClass().getClassLoader().getResource(fileName);
        return Files.readAllLines(Paths.get(file.toURI()), StandardCharsets.UTF_8);
    }
}
