package io.amicolon;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;

class QueriesExecutor
{
    private final EventTypeStatisticsPrinter eventTypeStatisticsPrinter = new EventTypeStatisticsPrinter();
    private final DataTransformer dataTransformer = new DataTransformer();

    void executeQueries(List<String> queriesResult, List<String> eventsInput, Map<String, List<Integer>> events, String outputFile) throws IOException
    {
        try (PrintWriter output = new PrintWriter(new FileWriter(outputFile)))
        {
            final long numberOfEvents = eventsInput.size();
            final Map<String, IntSummaryStatistics> eventsStats = dataTransformer.getSummaryStatisticsForEventProperties(events);

            for (String query : queriesResult)
            {
                executeQuery(query, numberOfEvents, output, eventsStats);
            }
        }
    }

    private void executeQuery(String query, long numberOfEvents, PrintWriter output, Map<String, IntSummaryStatistics> eventsStats)
    {
        String[] splitQuery = query.split(":");
        final String queryName = splitQuery[0];
        final String eventType = queryName.equals("count") ? null : splitQuery[1];

        switch (queryName)
        {
            case "count":
                eventTypeStatisticsPrinter.printCount(numberOfEvents, output);
                break;
            case "min":
                eventTypeStatisticsPrinter.printMin(eventType, eventsStats, output);
                break;
            case "max":
                eventTypeStatisticsPrinter.printMax(eventType, eventsStats, output);
                break;
            case "sum":
                eventTypeStatisticsPrinter.printSum(eventType, eventsStats, output);
                break;
            case "avg":
                eventTypeStatisticsPrinter.printAvg(eventType, eventsStats, output);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}
