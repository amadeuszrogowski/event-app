package io.amicolon;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

class DataTransformer
{
    Map<String, List<Integer>> getEventPropertyValueListGroupedByEventPropertyType(List<String> eventsInput)
    {
        return eventsInput.stream()
                .map(this::eventPropertiesAsKeyValuePairsList)
                .flatMap(this::newEventProperty)
                .collect(groupingBy(EventProperty::getType, mapping(EventProperty::getValue, toList())));
    }

    Map<String, IntSummaryStatistics> getSummaryStatisticsForEventProperties(Map<String, List<Integer>> events)
    {
        HashMap<String, IntSummaryStatistics> propertyTypeToPropertyStatsMap = new HashMap<>();
        events.forEach((key, value) -> propertyTypeToPropertyStatsMap.put(key, newIntSummaryStatistics(value)));
        return propertyTypeToPropertyStatsMap;
    }

    private List<String> eventPropertiesAsKeyValuePairsList(String line)
    {
        final String[] eventPropertyAsKeyValuePairArray = line.split(",");
        return Arrays.asList(eventPropertyAsKeyValuePairArray);
    }

    private Stream<EventProperty> newEventProperty(List<String> keyValueEventProperties)
    {
        return keyValueEventProperties.stream().map(EventProperty::new);
    }

    private IntSummaryStatistics newIntSummaryStatistics(List<Integer> values)
    {
        return values.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
    }

    private static final class EventProperty
    {
        final private String type;
        final private int value;

        public EventProperty(String property)
        {
            final String[] splitProperty = property.split("=");
            final String propertyType = splitProperty[0];
            final int propertyValue = Integer.parseInt(splitProperty[1]);

            this.type = propertyType;
            this.value = propertyValue;
        }

        public String getType()
        {
            return type;
        }

        public int getValue()
        {
            return value;
        }
    }
}
