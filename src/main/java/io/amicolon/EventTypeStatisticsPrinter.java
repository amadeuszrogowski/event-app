package io.amicolon;

import java.io.PrintWriter;
import java.util.IntSummaryStatistics;
import java.util.Map;

class EventTypeStatisticsPrinter
{
    void printCount(long count, PrintWriter output)
    {
        output.println("count = " + count);
    }

    void printMin(String eventType, Map<String, IntSummaryStatistics> eventStats, PrintWriter output)
    {
        final int minVal = eventStats.get(eventType).getMin();
        output.println("min:" + eventType + " = " + minVal);
    }

    void printMax(String eventType, Map<String, IntSummaryStatistics> eventStats, PrintWriter output)
    {
        final int maxVal = eventStats.get(eventType).getMax();
        output.println("max:" + eventType + " = " + maxVal);
    }

    void printSum(String eventType, Map<String, IntSummaryStatistics> eventStats, PrintWriter output)
    {
        final long sum = eventStats.get(eventType).getSum();
        output.println("sum:" + eventType + " = " + sum);
    }

    void printAvg(String eventType, Map<String, IntSummaryStatistics> eventStats, PrintWriter output)
    {
        final double avg = eventStats.get(eventType).getAverage();
        output.println("sum:" + eventType + " = " + avg);
    }
}
